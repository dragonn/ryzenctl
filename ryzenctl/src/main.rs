#[macro_use]
pub extern crate log;

use anyhow::Result;
use gumdrop::Options;
use ryzen_platform::{
    config::RyzenDProfiles,
    dbus::{self, ZSerialze},
    profile::{RyzenProfileModeKeys, RyzenProfilePowerKeys},
    settings::{RyzenSettings},
};

#[derive(Debug, Options)]
struct CliOptions {
    #[options(help = "print help message")]
    help: bool,

    #[options(help = "be verbose")]
    verbose: bool,

    #[options(command)]
    command: Option<Command>,
}

#[derive(Debug, Options)]
enum Command {
    #[options(help = "profile managment")]
    Profile(ProfileOptions),
    #[options(help = "current settings and stats")]
    Current(CurrentOptions),
}

#[derive(Debug, Options)]
struct CurrentOptions {
    #[options(help = "print help message")]
    help: bool,

    #[options(help = "select a setting to edit")]
    setting: Option<RyzenSettings>,

    #[options(help = "save current setting to current profile")]
    save: bool,

    #[options(help = "get cpu stats")]
    stats: bool,
}

#[derive(Debug, Options)]
struct ProfileOptions {
    #[options(help = "print help message")]
    help: bool,

    #[options(required, help = "select profile: [quiet, balanced, performance]")]
    profile: Option<RyzenDProfiles>,

    #[options(required, help = "set profile mode: [single, power]")]
    mode: Option<RyzenProfileModeKeys>,

    #[options(help = "select power profile: [ac, battery]")]
    power: Option<RyzenProfilePowerKeys>,

    #[options(help = "select a setting to edit")]
    setting: Option<RyzenSettings>,
}

#[tokio::main]
async fn main() -> Result<()> {
    if std::env::var("RUST_LOG").is_err() {
        std::env::set_var("RUST_LOG", "debug");
        std::env::set_var("RUST_BACKTRACE", "1");
    }

    env_logger::init();

    let opts = CliOptions::parse_args_default_or_exit();

    let dbus_interface = dbus::connect().await?;

    info!("dbus connected");

    #[allow(clippy::single_match)]
    match opts.command {
        Some(Command::Current(current)) => {
            if let Some(setting) = current.setting {
                dbus_interface.current_change_setting(ZSerialze(setting)).await?;
            }
            if current.save {
                dbus_interface.current_save().await?;
            }
            if current.stats {
                info!("got stats: {:#?}", dbus_interface.stats().await?);
            }
        }
        Some(Command::Profile(profile)) => {
            info!("got profile: {:?}", profile);
            match (
                profile.profile,
                profile.mode,
                profile.power,
                profile.setting,
            ) {
                (
                    Some(profile),
                    Some(mode),
                    power,
                    Some(setting),
                ) => {
                    dbus_interface.edit_profile(profile, mode, power, ZSerialze(setting)).await?;
                }
                _ => error!("invalid command line parameters, needs at least a profile and setting selected and profile mode")
            }
        }
        None => {}
    }

    Ok(())
}
