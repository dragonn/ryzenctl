pub const DBUS_NAME: &str = "org.ryzenctl.Daemon";

use std::fmt;

use anyhow::Result;
use serde::{
    de::{self, DeserializeOwned, Visitor},
    Deserialize, Serialize, Serializer,
};
use zbus::{dbus_proxy, Connection};

use crate::{
    config::RyzenDProfiles,
    profile::{RyzenProfileModeKeys, RyzenProfilePowerKeys},
    settings::RyzenSettings,
    status::RyzenCurrentValues,
};

#[dbus_proxy(
    interface = "org.ryzenctl.Daemon",
    default_service = "org.ryzenctl.Daemon",
    default_path = "/org/ryzenctl/Daemon"
)]

trait RyzenctlInterface {
    async fn edit_profile(
        &self,
        profile: RyzenDProfiles,
        mode: RyzenProfileModeKeys,
        power: Option<RyzenProfilePowerKeys>,
        setting: ZSerialze<RyzenSettings>,
    ) -> zbus::Result<()>;

    async fn current_change_setting(&self, setting: ZSerialze<RyzenSettings>) -> zbus::Result<()>;

    async fn current_save(&self) -> zbus::Result<()>;

    async fn stats(&self) -> zbus::Result<RyzenCurrentValues>;

    async fn version(&self) -> zbus::Result<String>;
}

pub async fn connect() -> Result<RyzenctlInterfaceProxy<'static>> {
    let connection = Connection::system().await?;
    Ok(RyzenctlInterfaceProxy::new(&connection).await?)
}

#[derive(Debug)]
pub struct ZSerialze<T: Serialize>(pub T);

impl<T: Serialize> Serialize for ZSerialze<T> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
        S::Error: serde::ser::Error,
    {
        //TODO add error handling
        serializer.serialize_str(&ron::to_string(&self.0).unwrap())
    }
}

impl<T: Serialize> zvariant::Type for ZSerialze<T> {
    fn signature() -> zvariant::Signature<'static> {
        zvariant::Signature::try_from("s").unwrap()
    }
}

#[derive(Debug)]
pub struct ZDeserialize<T: DeserializeOwned>(pub T);

impl<'de, T: DeserializeOwned> Deserialize<'de> for ZDeserialize<T> {
    fn deserialize<D>(deserializer: D) -> std::result::Result<Self, D::Error>
    where
        D: serde::Deserializer<'de>,
    {
        let string = deserializer.deserialize_string(StringVisitor)?;
        println!("got string: {:?}, type: {}", string, std::any::type_name::<T>());
        Ok(Self(ron::from_str(&string).unwrap()))
    }
}

impl<T: DeserializeOwned> zvariant::Type for ZDeserialize<T> {
    fn signature() -> zvariant::Signature<'static> {
        zvariant::Signature::try_from("s").unwrap()
    }
}

struct StringVisitor;

impl<'de> Visitor<'de> for StringVisitor {
    type Value = String;

    fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str("a RON formated string")
    }

    fn visit_string<E>(self, v: String) -> std::result::Result<Self::Value, E>
    where
        E: de::Error,
    {
        Ok(v)
    }

    fn visit_str<E>(self, v: &str) -> std::result::Result<Self::Value, E>
    where
        E: de::Error,
    {
        Ok(v.to_owned())
    }
}
