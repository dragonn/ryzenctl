use std::{
    collections::HashMap,
    hash::{Hash, Hasher},
    str::FromStr,
};

use derivative::Derivative;
use enum_kinds::EnumKind;
use serde::{Deserialize, Serialize};

#[derive(Derivative, Debug, Clone, Deserialize, Serialize, EnumKind)]
#[enum_kind(RyzenSettingsKeys, derive(Hash))]
pub enum RyzenSettings {
    ApuSkinTemp(u32),
    ApuSlowLimit(u32),
    DgpuSkinTempLimit(u32),
    FastLimit(u32),
    GfxClk(u32),
    Psio0Current(u32),
    Psio0SocCurrent(u32),
    Slow { limit: Option<u32>, time: Option<u32> },
    Stapm { limit: Option<u32>, time: Option<u32> },
    TctlTemp(u32),
    VrmCurrrent(u32),
    VrmMaxCurrent(u32),
    VrmSocCurrent(u32),
    VrmSocMaxCurrent(u32),
    CoreCurveOptimiser(CoreCurveOptimiser),
    PowerMode(RyzenPowerMode),
}

#[derive(Derivative, Debug, Clone, Deserialize, Serialize)]
pub enum RyzenPowerMode {
    MaxPerformance,
    PowerSaving,
}

impl PartialEq<RyzenSettings> for RyzenSettings {
    fn eq(&self, other: &RyzenSettings) -> bool {
        RyzenSettingsKeys::from(self) == RyzenSettingsKeys::from(other)
    }
}

impl Eq for RyzenSettings {}

impl Hash for RyzenSettings {
    fn hash<H: Hasher>(&self, state: &mut H) {
        RyzenSettingsKeys::from(self).hash(state);
    }
}

impl FromStr for RyzenSettings {
    type Err = ron::de::SpannedError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        ron::from_str(s)
    }
}

#[derive(Debug, Clone, Deserialize, Serialize)]
#[serde(untagged)]
pub enum CoreCurveOptimiser {
    All(i32),
    PerCore(HashMap<u32, i32>),
}
