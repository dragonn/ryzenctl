use std::collections::HashSet;

use anyhow::Result;
use serde::{Deserialize, Serialize};
use strum::EnumString;
use tokio::{
    fs::File,
    io::{AsyncReadExt, AsyncWriteExt},
};

use crate::{
    profile::{RyzenProfile, RyzenProfileMode},
    settings::RyzenSettings,
};

#[cfg(debug_assertions)]
const CONFIG_PATCH: &str = "./data/ryzend.ron";

#[cfg(not(debug_assertions))]
const CONFIG_PATCH: &str = "/etc/ryzend.ron";

#[derive(Debug, Deserialize, Serialize, EnumString, zvariant::Type)]
#[strum(serialize_all = "snake_case")]
pub enum RyzenDProfiles {
    Quiet,
    Balanced,
    Performance,
}

#[derive(Debug, Deserialize, Serialize)]
pub struct RyzenDConfig {
    #[serde(default)]
    pub export_mode: bool,

    pub quiet: RyzenProfileMode,
    pub balanced: RyzenProfileMode,
    pub performance: RyzenProfileMode,
}

impl Default for RyzenDConfig {
    fn default() -> Self {
        let mut profile_set = HashSet::new();
        profile_set.insert(RyzenSettings::ApuSkinTemp(30));
        profile_set.insert(RyzenSettings::ApuSlowLimit(30));
        profile_set.insert(RyzenSettings::Slow { limit: Some(20), time: None });
        let profile_set = RyzenProfile(profile_set);

        Self {
            export_mode: false,
            quiet: RyzenProfileMode::Single(profile_set.clone()),
            balanced: RyzenProfileMode::Power {
                ac: profile_set.clone(),
                battery: profile_set.clone(),
            },
            performance: RyzenProfileMode::Single(profile_set),
        }
    }
}

impl RyzenDConfig {
    pub async fn load() -> Result<Self> {
        let mut buffer = String::new();
        let mut file = File::open(CONFIG_PATCH).await?;
        file.read_to_string(&mut buffer).await?;
        Ok(ron::from_str(&buffer)?)
    }

    pub async fn save(&self) -> Result<()> {
        let mut file = File::create(CONFIG_PATCH).await?;
        //file.write_all(toml::to_string_pretty(self)?.as_bytes()).await?;
        file.write_all(
            ron::ser::to_string_pretty(self, ron::ser::PrettyConfig::default())?.as_bytes(),
        )
        .await?;
        Ok(())
    }
}
