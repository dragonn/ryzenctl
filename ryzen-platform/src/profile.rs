use std::{collections::HashSet, fmt, marker::PhantomData};

use anyhow::Result;
use enum_kinds::EnumKind;
use serde::{
    de::{Deserializer, MapAccess, Visitor},
    Deserialize, Serialize,
};
use strum::EnumString;

use crate::settings::RyzenSettings;

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct RyzenProfile(pub HashSet<RyzenSettings>);

#[derive(Debug, Deserialize, Serialize, EnumKind)]
#[enum_kind(
    RyzenProfileModeKeys,
    derive(Serialize, Deserialize, EnumString, Hash, zvariant::Type),
    serde(rename_all = "snake_case"),
    strum(serialize_all = "snake_case")
)]

pub enum RyzenProfileMode {
    Single(RyzenProfile),
    Power { ac: RyzenProfile, battery: RyzenProfile },
}

#[derive(Debug, Serialize, Deserialize, EnumString, Hash, zvariant::Type)]
#[serde(rename_all = "snake_case")]
#[strum(serialize_all = "snake_case")]
pub enum RyzenProfilePowerKeys {
    Ac,
    Battery,
}

impl From<crate::status::RyzenCurrentSettings> for RyzenProfile {
    fn from(cur_settings: crate::status::RyzenCurrentSettings) -> Self {
        let mut settings = HashSet::new();

        settings.insert(RyzenSettings::ApuSkinTemp(cur_settings.apu_skin_temp_limit.round() as _));
        settings.insert(RyzenSettings::ApuSlowLimit(cur_settings.apu_slow_limit.round() as _));
        settings.insert(RyzenSettings::DgpuSkinTempLimit(
            cur_settings.dgpu_skin_temp_limit.round() as _,
        ));
        settings.insert(RyzenSettings::FastLimit(cur_settings.fast_limit.round() as _));
        settings.insert(RyzenSettings::Slow {
            limit: Some(cur_settings.slow_limit.round() as _),
            time: Some(cur_settings.slow_time.round() as _),
        });
        settings.insert(RyzenSettings::Stapm {
            limit: Some(cur_settings.stapm_limit.round() as _),
            time: Some(cur_settings.stapm_time.round() as _),
        });
        settings.insert(RyzenSettings::TctlTemp(cur_settings.tctl_temp.round() as _));

        Self(settings)
    }
}
