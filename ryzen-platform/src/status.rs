use serde::{Deserialize, Serialize};
#[derive(Debug, Serialize, Deserialize, zvariant::Type)]
pub struct RyzenCurrentSettings {
    pub apu_skin_temp_limit: f32,
    pub apu_slow_limit: f32,
    pub dgpu_skin_temp_limit: f32,
    pub fast_limit: f32,
    pub gfx_clk: f32,
    pub psi0_current: f32,
    pub psi0soc_current: f32,
    pub slow_limit: f32,
    pub slow_time: f32,
    pub stapm_limit: f32,
    pub stapm_time: f32,
    pub tctl_temp: f32,
    pub vrm_current: f32,
    pub vrmmax_current: f32,
    pub vrmsoc_current: f32,
    pub vrmsocmax_current: f32,
}

#[derive(Debug, Clone, Serialize, Deserialize, zvariant::Type)]
pub struct RyzenCurrentValues {
    pub apu_skin_temp: f32,
    pub apu_slow: f32,
    pub bios_if_ver: i32,
    pub cclk_busy: f32,
    pub cclk_setpoint: f32,
    pub core_clk: Vec<f32>,
    pub core_power: Vec<f32>,
    pub core_temp: Vec<f32>,
    pub core_volt: Vec<f32>,
    //#[serde(with = "RyzenFamily")]
    //pub cpu_family: libryzenadj::RyzenFamily,
    pub dgpu_skin_temp: f32,
    pub fast: f32,
    pub fclk: f32,
    pub gfx_temp: f32,
    pub gfx_volt: f32,
    pub l3_clk: f32,
    pub l3_logic: f32,
    pub l3_temp: f32,
    pub l3_vddm: f32,
    pub mem_clk: f32,
    pub slow: f32,
    pub soc_power: f32,
    pub soc_volt: f32,
    pub socket_power: f32,
    pub stapm: f32,
    pub tctl_temp: f32,
    pub vrm_current: f32,
    pub vrmmax_current: f32,
    pub vrmsoc_current: f32,
    pub vrmsocmax_current: f32,
}

/*#[derive(Debug, Serialize, Deserialize)]
#[serde(remote = "libryzenadj::RyzenFamily")]
pub enum RyzenFamily {
    Unknow,
    Raven,
    Picassso,
    Renoir,
    Cezanne,
    Dali,
    Lucienne,
    Vangogh,
    Rembrandt,
}*/
