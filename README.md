# `ryzenctl` for AMD Ryzen

**WARNING:** Use at your own risk! Changing some of the exposed settings can lead to system unstabilty or can be even dangerous to your hardware.

**WARNING:** This project is still WIP, things will change and might not work/missing. Use only for testing/if you want to help wih development. 

`ryzenctl` is a utility for Linux to control AMD Ryzen CPU-s in a more advanced way, tied to power profiles present on many laptops. The finnal goal is to provide something like [AMD APU Tunning Utility](https://github.com/JamesCJ60/AMD-APU-Tuning-Utility) but tightly integrated with Linux.
It's core is provided by [RyzenAdj](https://github.com/FlyGoat/RyzenAdj)

## System requirements 

- secure boot needs to be disabled or use a shim, it will probaly not work with regular enabled secure boot
- systemd based distro
- rust installed via rustup

## Goals

1. To provide an interface for rootless control of AMD Ryzen CPU
2. Respect the users resources: be small, light, and fast

## Implemented

- [X] Basis system deamon
- [X] Reacting to power profile changes
- [X] Reacting to ac disconnect/connect changes
- [ ] Dbus interfaces
- [ ] CMD app
- [ ] GUI app

# BUILDING

Comming soon

## Installing

Comming soon

## Uninstalling

Comming soon