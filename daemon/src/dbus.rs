use ryzen_platform::{
    config::RyzenDProfiles,
    dbus::ZDeserialize,
    profile::{RyzenProfileModeKeys, RyzenProfilePowerKeys},
    settings::RyzenSettings,
    status::RyzenCurrentValues,
};
use tokio::sync::{
    mpsc::{unbounded_channel, UnboundedReceiver, UnboundedSender},
    oneshot::{channel as oneshot_channel, error::RecvError, Sender as OneshotSender},
};
use zbus::dbus_interface;

pub struct DbusServer(UnboundedSender<DbusEvent>);

#[derive(Debug)]
pub enum DbusEvent {
    ChangeSetting(RyzenSettings, OneshotSender<anyhow::Result<()>>),
    Stats(OneshotSender<anyhow::Result<RyzenCurrentValues>>),
    CurrentSave,
}

#[derive(thiserror::Error, Debug)]
pub enum DataStoreError {
    #[error("data store disconnected")]
    IOError(#[from] RecvError),
}

impl From<DataStoreError> for zbus::fdo::Error {
    fn from(e: DataStoreError) -> Self {
        match e {
            DataStoreError::IOError(e) => zbus::fdo::Error::IOError(format!("{e}")),
        }
    }
}

#[dbus_interface(name = "org.ryzenctl.Daemon")]
impl DbusServer {
    async fn edit_profile(
        &self,
        _profile: RyzenDProfiles,
        _mode: RyzenProfileModeKeys,
        _power: Option<RyzenProfilePowerKeys>,
        _setting: ZDeserialize<RyzenSettings>,
    ) -> zbus::fdo::Result<()> {
        Ok(())
    }

    async fn current_change_setting(
        &self,
        setting: ZDeserialize<RyzenSettings>,
    ) -> zbus::fdo::Result<()> {
        info!("got setting: {:?}", setting);
        let (tx, rx) = oneshot_channel();
        self.0.send(DbusEvent::ChangeSetting(setting.0, tx)).ok();
        match rx.await {
            Ok(Ok(_)) => Ok(()),
            Ok(Err(e)) => Err(zbus::fdo::Error::IOError(e.to_string())),
            Err(e) => Err(zbus::fdo::Error::Disconnected(e.to_string())),
        }
    }

    async fn current_save(&self) -> zbus::fdo::Result<()> {
        info!("saving current setting to profile");
        self.0.send(DbusEvent::CurrentSave).ok();
        Ok(())
    }

    async fn stats(&self) -> zbus::fdo::Result<RyzenCurrentValues> {
        let (tx, rx) = oneshot_channel();
        self.0.send(DbusEvent::Stats(tx)).ok();
        match rx.await {
            Ok(Ok(stats)) => Ok(stats),
            Ok(Err(e)) => Err(zbus::fdo::Error::IOError(e.to_string())),
            Err(e) => Err(zbus::fdo::Error::Disconnected(e.to_string())),
        }
    }

    async fn version(&self) -> zbus::fdo::Result<String> {
        Ok(env!("CARGO_PKG_VERSION").to_owned())
    }
}

impl DbusServer {
    pub fn new() -> (Self, UnboundedReceiver<DbusEvent>) {
        let (event_sender, event_reciver) = unbounded_channel();
        (DbusServer(event_sender), event_reciver)
    }
}
