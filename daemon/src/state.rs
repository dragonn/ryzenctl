use anyhow::Result as AnyResult;
use derivative::Derivative;
use futures::stream::StreamExt;
use notify::{Event, INotifyWatcher, RecursiveMode, Watcher};
use strum::EnumString;
use tokio::{
    fs,
    sync::mpsc::{unbounded_channel, UnboundedReceiver},
};
use upower_dbus::UPowerProxy;

static POWER_PROFILES_PATCH: &str = "/sys/firmware/acpi/platform_profile";

#[derive(Debug, EnumString, Clone, Copy)]
#[strum(serialize_all = "lowercase")]
pub enum PowerProfile {
    Quiet,
    Balanced,
    Performance,
}

#[derive(Derivative)]
#[derivative(Debug)]
pub struct State<'a> {
    pub on_battery: bool,
    pub power_profile: PowerProfile,

    #[allow(dead_code)]
    #[derivative(Debug = "ignore")]
    power_profiles_inotify: INotifyWatcher,
    #[derivative(Debug = "ignore")]
    power_profiles_inotify_reciver: UnboundedReceiver<Result<Event, notify::Error>>,

    #[allow(dead_code)]
    #[derivative(Debug = "ignore")]
    system_bus_client: zbus::Connection,
    #[allow(dead_code)]
    #[derivative(Debug = "ignore")]
    upower_proxy: UPowerProxy<'a>,
    #[derivative(Debug = "ignore")]
    upower_on_battery_stream: zbus::PropertyStream<'a, bool>,
}

impl<'a> State<'a> {
    pub async fn new() -> AnyResult<State<'a>> {
        let (power_profiles_inotify_sender, power_profiles_inotify_reciver) = unbounded_channel();
        let mut power_profiles_inotify = notify::recommended_watcher(move |res| {
            power_profiles_inotify_sender.send(res).expect("sending inotify failed");
        })?;

        power_profiles_inotify.watch(POWER_PROFILES_PATCH.as_ref(), RecursiveMode::NonRecursive)?;
        let power_profile = Self::power_profile().await?;

        let system_bus_client = zbus::Connection::system().await?;
        let upower_proxy = UPowerProxy::new(&system_bus_client).await?;
        let on_battery = upower_proxy.on_battery().await?;

        let upower_on_battery_stream = upower_proxy.receive_on_battery_changed().await;

        Ok(Self {
            on_battery,
            power_profile,

            power_profiles_inotify,
            power_profiles_inotify_reciver,
            system_bus_client,
            upower_proxy,
            upower_on_battery_stream,
        })
    }

    async fn power_profile() -> AnyResult<PowerProfile> {
        let power_profile_buffer = fs::read(POWER_PROFILES_PATCH).await?;
        let power_profile_str = String::from_utf8_lossy(&power_profile_buffer);
        Ok(PowerProfile::try_from(power_profile_str.trim_end())?)
    }

    pub async fn listen(&mut self) -> AnyResult<()> {
        tokio::select! {
            power_profiles_event = self.power_profiles_inotify_reciver.recv() => {
                if let Some(power_profiles_event) = power_profiles_event {
                    power_profiles_event?;
                    self.power_profile = Self::power_profile().await?;
                }
            }
            on_battery_changed = self.upower_on_battery_stream.next() => {
                if let Some(on_battery_changed) = on_battery_changed {
                    self.on_battery = on_battery_changed.get().await?;
                }
            }
        }
        Ok(())
    }
}
