use anyhow::Result as AnyResult;
use ryzen_platform::{
    config::RyzenDConfig,
    profile::{RyzenProfile, RyzenProfileMode},
    settings::RyzenSettings,
};

use crate::{
    ryzenadj::RyzenAdj,
    state::{PowerProfile, State},
};

#[derive(Debug)]
pub struct Profile {
    on_battery: bool,
    power_profile: PowerProfile,
}

impl Profile {
    pub fn new(state: &State) -> Self {
        Self { on_battery: state.on_battery, power_profile: state.power_profile }
    }

    pub fn current_profile_mut<'a>(&self, config: &'a mut RyzenDConfig) -> &'a mut RyzenProfile {
        let profile = match self.power_profile {
            PowerProfile::Balanced => &mut config.balanced,
            PowerProfile::Quiet => &mut config.quiet,
            PowerProfile::Performance => &mut config.performance,
        };

        match profile {
            RyzenProfileMode::Single(profile) => profile,
            RyzenProfileMode::Power { ac, battery } => {
                if self.on_battery {
                    battery
                } else {
                    ac
                }
            }
        }
    }

    pub fn refresh(&mut self, state: &State) {
        self.on_battery = state.on_battery;
        self.power_profile = state.power_profile;
    }

    pub fn apply<'a>(&self, config: &'a mut RyzenDConfig, ryzenadj: &RyzenAdj) -> AnyResult<()> {
        for setting in self.current_profile_mut(config).0.iter() {
            info!("aplaying setting: {:?}", setting);
            ryzenadj.apply_setting(setting)?;
        }
        Ok(())
    }

    pub fn change<'a>(&self, config: &'a mut RyzenDConfig, setting: RyzenSettings) {
        self.current_profile_mut(config).0.replace(setting);
    }
}
