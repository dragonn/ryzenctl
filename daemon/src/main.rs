#[macro_use]
pub extern crate log;

use anyhow::Result;
use ryzen_platform::{config::RyzenDConfig, dbus::DBUS_NAME};
use zbus::ConnectionBuilder;

mod dbus;
mod profile;
mod ryzenadj;
mod state;

#[tokio::main]
async fn main() -> Result<()> {
    if std::env::var("RUST_LOG").is_err() {
        std::env::set_var("RUST_LOG", "debug");
        std::env::set_var("RUST_BACKTRACE", "1");
    }

    env_logger::init();

    // Start zbus server
    let (dbus_server, mut event_reciver) = dbus::DbusServer::new();
    info!("creating dbus server");
    let _dbus_connection = ConnectionBuilder::system()?
        .name(DBUS_NAME)?
        .internal_executor(true)
        .serve_at(format!("/{}", DBUS_NAME.replace('.', "/")), dbus_server)?
        .build()
        .await?;

    let ryzenadj = ryzenadj::RyzenAdj::new()?;
    let current_profile: ryzen_platform::profile::RyzenProfile =
        ryzenadj.current_settings()?.into();
    info!("current settings: {}", ron::to_string(&current_profile)?);

    let mut config = RyzenDConfig::load().await?;
    info!("got config: {:?}", config);

    let mut state = state::State::new().await?;

    let mut current_profile = profile::Profile::new(&state);
    current_profile.apply(&mut config, &ryzenadj)?;

    info!("found profile: {:?}", current_profile);

    info!("entering main loop");

    loop {
        tokio::select! {
            _ = state.listen() => {
                info!("state changed: {:?}", state);
                current_profile.refresh(&state);
                current_profile.apply(&mut config, &ryzenadj)?;
            }
            event = event_reciver.recv() => {
                if let Some(event) = event {
                    //info!("got event: {:?}", event);
                    match event {
                        dbus::DbusEvent::ChangeSetting(setting, tx_result) =>  {
                            tx_result.send(ryzenadj.apply_setting(&setting)).ok();
                            current_profile.change(&mut config, setting);
                            config.save().await?;
                        },
                        dbus::DbusEvent::CurrentSave => {
                            warn!("saving current settings to profile");
                        }
                        dbus::DbusEvent::Stats(tx_result) => {
                            tx_result.send(ryzenadj.current_values()).ok();
                        }
                    }
                }
            }
        }
    }
}
