use anyhow::Result;
use ryzen_platform::{
    settings::{CoreCurveOptimiser, RyzenPowerMode, RyzenSettings},
    status::{RyzenCurrentSettings, RyzenCurrentValues},
};

pub struct RyzenAdj {
    inner: libryzenadj::RyzenAdj,
}

impl RyzenAdj {
    pub fn new() -> Result<Self> {
        Ok(Self { inner: libryzenadj::RyzenAdj::new()? })
    }

    pub fn current_values(&self) -> Result<RyzenCurrentValues> {
        self.inner.refresh()?;
        let cpus = num_cpus::get_physical() as u32;
        Ok(RyzenCurrentValues {
            apu_skin_temp: self.inner.get_apu_skin_temp_value()?,
            apu_slow: self.inner.get_apu_slow_value()?,
            bios_if_ver: self.inner.get_bios_if_ver()?,
            cclk_busy: self.inner.get_cclk_busy_value()?,
            cclk_setpoint: self.inner.get_cclk_setpoint()?,
            core_clk: (0..cpus)
                .into_iter()
                .map(|c| self.inner.get_core_clk(c))
                .collect::<Result<Vec<_>, _>>()?,
            core_power: (0..cpus)
                .into_iter()
                .map(|c| self.inner.get_core_power(c))
                .collect::<Result<Vec<_>, _>>()?,
            core_temp: (0..cpus)
                .into_iter()
                .map(|c| self.inner.get_core_temp(c))
                .collect::<Result<Vec<_>, _>>()?,
            core_volt: (0..cpus)
                .into_iter()
                .map(|c| self.inner.get_core_volt(c))
                .collect::<Result<Vec<_>, _>>()?,
            //cpu_family: self.inner.get_cpu_family()?,
            dgpu_skin_temp: self.inner.get_dgpu_skin_temp_value()?,
            fast: self.inner.get_fast_value()?,
            fclk: self.inner.get_fclk()?,
            gfx_temp: self.inner.get_gfx_temp()?,
            gfx_volt: self.inner.get_gfx_volt()?,
            l3_clk: self.inner.get_l3_clk()?,
            l3_logic: self.inner.get_l3_logic()?,
            l3_temp: self.inner.get_l3_temp()?,
            l3_vddm: self.inner.get_l3_vddm()?,
            mem_clk: self.inner.get_mem_clk()?,
            slow: self.inner.get_slow_value()?,
            soc_power: self.inner.get_soc_power()?,
            soc_volt: self.inner.get_soc_volt()?,
            socket_power: self.inner.get_socket_power()?,
            stapm: self.inner.get_stapm_value()?,
            tctl_temp: self.inner.get_tctl_temp_value()?,
            vrm_current: self.inner.get_vrm_current_value()?,
            vrmmax_current: self.inner.get_vrmmax_current_value()?,
            vrmsoc_current: self.inner.get_vrmsoc_current_value()?,
            vrmsocmax_current: self.inner.get_vrmsocmax_current_value()?,
        })
    }

    pub fn current_settings(&self) -> Result<RyzenCurrentSettings> {
        self.inner.refresh()?;
        Ok(RyzenCurrentSettings {
            apu_skin_temp_limit: self.inner.get_apu_skin_temp_limit()?,
            apu_slow_limit: self.inner.get_apu_slow_limit()?,
            dgpu_skin_temp_limit: self.inner.get_dgpu_skin_temp_limit()?,
            fast_limit: self.inner.get_fast_limit()?,
            gfx_clk: self.inner.get_gfx_clk()?,
            psi0_current: self.inner.get_psi0_current()?,
            psi0soc_current: self.inner.get_psi0soc_current()?,
            slow_limit: self.inner.get_slow_limit()?,
            slow_time: self.inner.get_slow_time()?,
            stapm_limit: self.inner.get_stapm_limit()?,
            stapm_time: self.inner.get_stapm_time()?,
            tctl_temp: self.inner.get_tctl_temp()?,
            vrm_current: self.inner.get_vrm_current()?,
            vrmmax_current: self.inner.get_vrmmax_current()?,
            vrmsoc_current: self.inner.get_vrmsoc_current()?,
            vrmsocmax_current: self.inner.get_vrmsocmax_current()?,
        })
    }

    pub fn apply_setting(&self, setting: &RyzenSettings) -> Result<()> {
        match setting {
            RyzenSettings::ApuSkinTemp(value) => self.inner.set_apu_skin_temp_limit(*value)?,
            RyzenSettings::ApuSlowLimit(value) => self.inner.set_apu_slow_limit(*value)?,
            RyzenSettings::DgpuSkinTempLimit(value) => {
                self.inner.set_dgpu_skin_temp_limit(*value)?
            }
            RyzenSettings::FastLimit(value) => self.inner.set_fast_limit(*value)?,
            RyzenSettings::GfxClk(value) => self.inner.set_gfx_clk(*value)?,
            RyzenSettings::Psio0Current(value) => self.inner.set_psi0_current(*value)?,
            RyzenSettings::Psio0SocCurrent(value) => self.inner.set_psi0soc_current(*value)?,
            RyzenSettings::Slow { limit, time } => {
                if let Some(limit) = limit {
                    self.inner.set_slow_limit(*limit)?;
                }
                if let Some(time) = time {
                    self.inner.set_slow_time(*time)?;
                }
            }
            RyzenSettings::Stapm { limit, time } => {
                if let Some(limit) = limit {
                    self.inner.set_stapm_limit(*limit)?;
                }
                if let Some(time) = time {
                    self.inner.set_stapm_time(*time)?;
                }
            }
            RyzenSettings::TctlTemp(value) => self.inner.set_tctl_temp(*value)?,
            RyzenSettings::VrmCurrrent(value) => self.inner.set_vrm_current(*value)?,
            RyzenSettings::VrmMaxCurrent(value) => self.inner.set_vrmmax_current(*value)?,
            RyzenSettings::VrmSocCurrent(value) => self.inner.set_vrmsoc_current(*value)?,
            RyzenSettings::VrmSocMaxCurrent(value) => self.inner.set_vrmsocmax_current(*value)?,
            RyzenSettings::CoreCurveOptimiser(value) => match value {
                CoreCurveOptimiser::All(value) => self.inner.set_coall(*value)?,
                CoreCurveOptimiser::PerCore(value) => {
                    self.inner.set_coall(0);
                    for (core, value) in value {
                        self.inner.set_coper(*core, *value)?;
                    }
                }
            },
            RyzenSettings::PowerMode(mode) => match mode {
                RyzenPowerMode::MaxPerformance => self.inner.set_max_performance()?,
                RyzenPowerMode::PowerSaving => self.inner.set_power_saving()?,
            },
        }
        Ok(())
    }
}
