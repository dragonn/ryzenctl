use std::{sync::Arc, time::Duration};

use anyhow::anyhow;
use iced::{
    subscription,
    widget::{column, container, scrollable, text, Column, Row},
    Alignment, Element, Length, Subscription,
};
use iced_aw::native::{wrap_horizontal, wrap_vertical};
use ryzen_platform::{dbus::RyzenctlInterfaceProxy, status::RyzenCurrentValues};
use tokio::time::sleep;

use self::core::Core;
use crate::Message;

mod core;

#[derive(Debug)]
pub struct Stats {
    refresh: Duration,
    dbus: RyzenctlInterfaceProxy<'static>,
    stats: Option<RyzenCurrentValues>,

    cores: Vec<Core>,
}

impl Stats {
    pub fn new(dbus: RyzenctlInterfaceProxy<'static>) -> Self {
        Stats { refresh: Duration::from_millis(50), stats: None, cores: vec![], dbus }
    }

    pub fn update(&mut self, message: StatsMessage) {
        match message {
            StatsMessage::Refresh(stats) => {
                if self.cores.len() != stats.core_clk.len() {
                    self.cores = stats
                        .core_clk
                        .iter()
                        .enumerate()
                        .map(|(i, _)| Core::new(i, &stats))
                        .collect();
                } else {
                    self.cores.iter_mut().enumerate().for_each(|(i, c)| c.update(i, &stats))
                }
                self.stats = Some(stats)
            }
        }
    }

    pub fn subscription(&self) -> Subscription<Message> {
        let refresh = self.refresh;
        let dbus = self.dbus.clone();
        subscription::unfold(std::any::TypeId::of::<Stats>(), State::Getting, move |state| {
            stats(state, refresh, dbus.clone())
        })
        .map(|r| {
            if let Some(r) = r {
                Message::Stats(StatsMessage::Refresh(r))
            } else {
                Message::DbusError(Arc::new(anyhow!("no stats found")))
            }
        })
    }

    pub fn view(&self) -> Element<Message> {
        if let Some(_stats) = &self.stats {
            let columns = column![
                wrap_horizontal(self.cores.iter().map(|c| c.view()).collect()).spacing(10.0),
            ]
            .width(Length::Shrink)
            .align_items(Alignment::Center)
            .spacing(10);

            container(scrollable(columns)).width(Length::Shrink).height(Length::Shrink).into()
        } else {
            Column::new()
                .push(text("stats not found"))
                .spacing(10)
                .padding(10)
                .align_items(Alignment::Center)
                .into()
        }
    }
}

async fn stats(
    state: State,
    refresh: Duration,
    dbus: RyzenctlInterfaceProxy<'static>,
) -> (Option<RyzenCurrentValues>, State) {
    match state {
        State::Getting => {
            if let Ok(stats) = dbus.stats().await {
                (Some(stats), State::Sleeping)
            } else {
                (None, State::Sleeping)
            }
        }
        State::Sleeping => {
            sleep(refresh).await;
            (None, State::Getting)
        }
    }
}

pub enum State {
    Getting,
    Sleeping,
}

#[derive(Debug, Clone)]
pub enum StatsMessage {
    Refresh(RyzenCurrentValues),
}
