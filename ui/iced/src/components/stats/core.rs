use iced::{
    color,
    widget::{
        column,
        container::{self, Appearance, StyleSheet},
        row, space, text, Container, Space, Text,
    },
    Element, Length, Theme,
};
use iced_aw::{grid, helpers::card, CardStyles, Grid};
use ryzen_platform::status::RyzenCurrentValues;

use crate::Message;

#[derive(Debug)]
struct CoreValue {
    current: f32,
    min: f32,
    max: f32,

    unit: &'static str,
    precision: usize,
}

impl CoreValue {
    pub fn new(value: f32, unit: &'static str, precision: usize) -> Self {
        Self { current: value, min: value, max: value, unit, precision }
    }

    pub fn update(&mut self, value: f32) {
        self.current = value;
        if self.min > value {
            self.min = value;
        }

        if self.max < value {
            self.max = value;
        }
    }

    pub fn view<'a>(&'a self, mut grid: Grid<'a, Message>) -> Grid<'a, Message> {
        let precision = self.precision;
        let base_size = 10;
        grid = grid.push(
            text(format!("{}: {:.precision$} ", self.unit, self.current)).size(base_size * 2),
        );
        grid.push(
            Container::new(column![
                text(format!("+ {:.precision$}", self.max)).size(base_size),
                text(format!("- {:.precision$}", self.min)).size(base_size)
            ])
            .style(iced::theme::Container::Custom(Box::new(CoreContainerStyle)))
            .padding(3)
            .width(40),
        )
    }
}

#[derive(Debug)]
pub struct Core {
    index: usize,
    core_clk: CoreValue,
    core_power: CoreValue,
    core_volt: CoreValue,
    core_temp: CoreValue,
}

impl Core {
    pub fn new(index: usize, values: &RyzenCurrentValues) -> Self {
        Core {
            index,
            core_clk: CoreValue::new(values.core_clk[index], "GHz", 1),
            core_power: CoreValue::new(values.core_power[index], "W", 2),
            core_volt: CoreValue::new(values.core_volt[index], "V", 3),
            core_temp: CoreValue::new(values.core_temp[index], "C", 1),
        }
    }

    pub fn update(&mut self, index: usize, values: &RyzenCurrentValues) {
        self.core_clk.update(values.core_clk[index]);
        self.core_power.update(values.core_power[index]);
        self.core_volt.update(values.core_volt[index]);
        self.core_temp.update(values.core_temp[index]);
    }

    pub fn view(&self) -> Element<Message> {
        let mut grid = grid!().strategy(iced_aw::Strategy::Columns(2));
        grid = self.core_clk.view(grid);
        grid = self.core_volt.view(grid);
        grid = self.core_temp.view(grid);
        grid = self.core_power.view(grid);
        card(Text::new(format!("Core{}", self.index)).size(18), Container::new(grid))
            .style(CardStyles::Light)
            .width(Length::Fixed(140.0))
            .into()
    }
}

pub struct CoreContainerStyle;

impl container::StyleSheet for CoreContainerStyle {
    type Style = Theme;

    fn appearance(&self, _: &Self::Style) -> Appearance {
        Appearance {
            border_width: 1.0,
            border_radius: 2.0.into(),
            border_color: color!(0xFF, 0xff, 0xff),
            ..Default::default()
        }
    }
}
