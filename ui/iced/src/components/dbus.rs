use std::{sync::Arc, time::Duration};

use anyhow::{anyhow, Error, Result};
use iced::{
    subscription,
    widget::{text, Column},
    Alignment, Element, Subscription,
};
use ryzen_platform::dbus::RyzenctlInterfaceProxy;
use tokio::time::sleep;

use crate::Message;

pub fn connection() -> iced::Subscription<Option<Result<RyzenctlInterfaceProxy<'static>>>> {
    subscription::unfold(std::any::TypeId::of::<Dbus>(), State::Connecting, move |state| {
        connect(state)
    })
}

#[derive(Debug)]
pub struct Dbus {
    err: Option<Arc<Error>>,
}

impl Dbus {
    pub fn new() -> Self {
        Dbus { err: None }
    }

    pub fn error(&mut self, err: Arc<Error>) {
        self.err = Some(err)
    }

    pub fn subscription(&self) -> Subscription<Message> {
        connection().map(|r| match r {
            Some(Ok(c)) => Message::DbusConnected(c),
            None => Message::DbusError(Arc::new(anyhow!("dbus connection not found"))),
            Some(Err(e)) => Message::DbusError(e.into()),
        })
    }

    pub fn view(&self) -> Element<Message> {
        let message = if let Some(err) = &self.err {
            text(format!("error: {}", err))
        } else {
            text("connecting...")
        };

        Column::new().push(message).spacing(10).padding(10).align_items(Alignment::Center).into()
    }
}

async fn connect(state: State) -> (Option<Result<RyzenctlInterfaceProxy<'static>>>, State) {
    match state {
        State::Connecting | State::Error => {
            info!("connecting to dbus");
            if let State::Error = state {
                sleep(Duration::from_secs(1)).await;
            } else {
                sleep(Duration::from_millis(500)).await;
            }

            match ryzen_platform::dbus::connect().await {
                Ok(dbus_connection) => match dbus_connection.version().await {
                    Ok(version) => {
                        info!("got version: {}", version);
                        (Some(Ok(dbus_connection)), State::Connected)
                    }
                    Err(err) => {
                        error!("getting version failed: {}", err);

                        (Some(Err(err.into())), State::Error)
                    }
                },
                Err(err) => {
                    error!("connecting failed: {}", err);
                    (Some(Err(err)), State::Error)
                }
            }
        }
        State::Connected => iced::futures::future::pending().await,
    }
}

pub enum State {
    Connecting,
    Connected,
    Error,
}
