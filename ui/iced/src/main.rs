#[macro_use]
pub extern crate log;

use std::{rc::Rc, sync::Arc};

use iced::{
    executor, widget::container, Application, Command, Element, Length, Settings, Subscription,
    Theme,
};

mod components;

use components::{Dbus, Stats, StatsMessage};
use ryzen_platform::dbus::RyzenctlInterfaceProxy;

pub fn main() -> iced::Result {
    if std::env::var("RUST_LOG").is_err() {
        std::env::set_var(
            "RUST_LOG",
            "debug,wgpu_core=warn,wgpu_hal=warn,naga=warn,iced_wgpu=warn,winit=warn,iced_winit=warn,cosmic_text=warn",
        );
        std::env::set_var("RUST_BACKTRACE", "1");
    }

    env_logger::init();
    RyzenUi::run(Settings::default())
}

#[derive(Debug)]
struct RyzenUi {
    dbus: Option<RyzenctlInterfaceProxy<'static>>,
    dbus_connector: Dbus,
    stats: Option<Stats>,
    theme: Theme,
}

#[derive(Debug, Clone)]
pub enum Message {
    DbusConnected(RyzenctlInterfaceProxy<'static>),
    DbusError(Arc<anyhow::Error>),
    Stats(StatsMessage),
    None,
}

impl Application for RyzenUi {
    type Message = Message;
    type Theme = Theme;
    type Executor = executor::Default;
    type Flags = ();

    fn new(_flags: ()) -> (RyzenUi, Command<Message>) {
        (
            RyzenUi { dbus: None, dbus_connector: Dbus::new(), theme: Theme::Dark, stats: None },
            Command::none(),
        )
    }

    fn title(&self) -> String {
        String::from("RyzenUI")
    }

    fn update(&mut self, message: Message) -> Command<Message> {
        match message {
            Message::DbusConnected(dbus) => {
                info!("dbus connected: {:?}", dbus);
                self.stats = Some(Stats::new(dbus.clone()));

                self.dbus = Some(dbus);
            }
            Message::DbusError(err) => {
                self.dbus_connector.error(err);
            }
            Message::Stats(stats) => {
                self.stats.as_mut().unwrap().update(stats);
            }
            _ => {}
        };

        Command::none()
    }

    fn subscription(&self) -> Subscription<Message> {
        if let Some(stats) = &self.stats {
            stats.subscription()
        } else {
            self.dbus_connector.subscription()
        }
    }

    fn view(&self) -> Element<Message> {
        if let Some(stats) = &self.stats {
            container(stats.view())
                .width(Length::Fill)
                .height(Length::Fill)
                .center_x()
                .center_y()
                .into()
        } else {
            container(self.dbus_connector.view())
                .width(Length::Fill)
                .height(Length::Fill)
                .center_x()
                .center_y()
                .into()
        }
    }

    fn theme(&self) -> Theme {
        self.theme.clone()
    }

    fn scale_factor(&self) -> f64 {
        0.85
    }
}
