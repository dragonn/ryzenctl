// coverage:ignore-file
// GENERATED CODE - DO NOT MODIFY BY HAND
// ignore_for_file: type=lint
// ignore_for_file: unused_element, deprecated_member_use, deprecated_member_use_from_same_package, use_function_type_syntax_for_parameters, unnecessary_const, avoid_init_to_null, invalid_override_different_default_values_named, prefer_expression_function_bodies, annotate_overrides, invalid_annotation_target, unnecessary_question_mark

part of 'bridge_definitions.dart';

// **************************************************************************
// FreezedGenerator
// **************************************************************************

T _$identity<T>(T value) => value;

final _privateConstructorUsedError = UnsupportedError(
    'It seems like you constructed your class using `MyClass._()`. This constructor is only meant to be used by freezed and you are not supposed to need it nor use it.\nPlease check the documentation here for more information: https://github.com/rrousselGit/freezed#custom-getters-and-methods');

/// @nodoc
mixin _$RyzenInterfaceState {
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() disconnected,
    required TResult Function(RyzenCurrentValues field0) connected,
    required TResult Function(String field0) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? disconnected,
    TResult? Function(RyzenCurrentValues field0)? connected,
    TResult? Function(String field0)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? disconnected,
    TResult Function(RyzenCurrentValues field0)? connected,
    TResult Function(String field0)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RyzenInterfaceState_Disconnected value) disconnected,
    required TResult Function(RyzenInterfaceState_Connected value) connected,
    required TResult Function(RyzenInterfaceState_Error value) error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(RyzenInterfaceState_Disconnected value)? disconnected,
    TResult? Function(RyzenInterfaceState_Connected value)? connected,
    TResult? Function(RyzenInterfaceState_Error value)? error,
  }) =>
      throw _privateConstructorUsedError;
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RyzenInterfaceState_Disconnected value)? disconnected,
    TResult Function(RyzenInterfaceState_Connected value)? connected,
    TResult Function(RyzenInterfaceState_Error value)? error,
    required TResult orElse(),
  }) =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class $RyzenInterfaceStateCopyWith<$Res> {
  factory $RyzenInterfaceStateCopyWith(RyzenInterfaceState value, $Res Function(RyzenInterfaceState) then) =
      _$RyzenInterfaceStateCopyWithImpl<$Res, RyzenInterfaceState>;
}

/// @nodoc
class _$RyzenInterfaceStateCopyWithImpl<$Res, $Val extends RyzenInterfaceState>
    implements $RyzenInterfaceStateCopyWith<$Res> {
  _$RyzenInterfaceStateCopyWithImpl(this._value, this._then);

  // ignore: unused_field
  final $Val _value;
  // ignore: unused_field
  final $Res Function($Val) _then;
}

/// @nodoc
abstract class _$$RyzenInterfaceState_DisconnectedCopyWith<$Res> {
  factory _$$RyzenInterfaceState_DisconnectedCopyWith(
          _$RyzenInterfaceState_Disconnected value, $Res Function(_$RyzenInterfaceState_Disconnected) then) =
      __$$RyzenInterfaceState_DisconnectedCopyWithImpl<$Res>;
}

/// @nodoc
class __$$RyzenInterfaceState_DisconnectedCopyWithImpl<$Res>
    extends _$RyzenInterfaceStateCopyWithImpl<$Res, _$RyzenInterfaceState_Disconnected>
    implements _$$RyzenInterfaceState_DisconnectedCopyWith<$Res> {
  __$$RyzenInterfaceState_DisconnectedCopyWithImpl(
      _$RyzenInterfaceState_Disconnected _value, $Res Function(_$RyzenInterfaceState_Disconnected) _then)
      : super(_value, _then);
}

/// @nodoc

class _$RyzenInterfaceState_Disconnected implements RyzenInterfaceState_Disconnected {
  const _$RyzenInterfaceState_Disconnected();

  @override
  String toString() {
    return 'RyzenInterfaceState.disconnected()';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) || (other.runtimeType == runtimeType && other is _$RyzenInterfaceState_Disconnected);
  }

  @override
  int get hashCode => runtimeType.hashCode;

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() disconnected,
    required TResult Function(RyzenCurrentValues field0) connected,
    required TResult Function(String field0) error,
  }) {
    return disconnected();
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? disconnected,
    TResult? Function(RyzenCurrentValues field0)? connected,
    TResult? Function(String field0)? error,
  }) {
    return disconnected?.call();
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? disconnected,
    TResult Function(RyzenCurrentValues field0)? connected,
    TResult Function(String field0)? error,
    required TResult orElse(),
  }) {
    if (disconnected != null) {
      return disconnected();
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RyzenInterfaceState_Disconnected value) disconnected,
    required TResult Function(RyzenInterfaceState_Connected value) connected,
    required TResult Function(RyzenInterfaceState_Error value) error,
  }) {
    return disconnected(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(RyzenInterfaceState_Disconnected value)? disconnected,
    TResult? Function(RyzenInterfaceState_Connected value)? connected,
    TResult? Function(RyzenInterfaceState_Error value)? error,
  }) {
    return disconnected?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RyzenInterfaceState_Disconnected value)? disconnected,
    TResult Function(RyzenInterfaceState_Connected value)? connected,
    TResult Function(RyzenInterfaceState_Error value)? error,
    required TResult orElse(),
  }) {
    if (disconnected != null) {
      return disconnected(this);
    }
    return orElse();
  }
}

abstract class RyzenInterfaceState_Disconnected implements RyzenInterfaceState {
  const factory RyzenInterfaceState_Disconnected() = _$RyzenInterfaceState_Disconnected;
}

/// @nodoc
abstract class _$$RyzenInterfaceState_ConnectedCopyWith<$Res> {
  factory _$$RyzenInterfaceState_ConnectedCopyWith(
          _$RyzenInterfaceState_Connected value, $Res Function(_$RyzenInterfaceState_Connected) then) =
      __$$RyzenInterfaceState_ConnectedCopyWithImpl<$Res>;
  @useResult
  $Res call({RyzenCurrentValues field0});
}

/// @nodoc
class __$$RyzenInterfaceState_ConnectedCopyWithImpl<$Res>
    extends _$RyzenInterfaceStateCopyWithImpl<$Res, _$RyzenInterfaceState_Connected>
    implements _$$RyzenInterfaceState_ConnectedCopyWith<$Res> {
  __$$RyzenInterfaceState_ConnectedCopyWithImpl(
      _$RyzenInterfaceState_Connected _value, $Res Function(_$RyzenInterfaceState_Connected) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? field0 = null,
  }) {
    return _then(_$RyzenInterfaceState_Connected(
      null == field0
          ? _value.field0
          : field0 // ignore: cast_nullable_to_non_nullable
              as RyzenCurrentValues,
    ));
  }
}

/// @nodoc

class _$RyzenInterfaceState_Connected implements RyzenInterfaceState_Connected {
  const _$RyzenInterfaceState_Connected(this.field0);

  @override
  final RyzenCurrentValues field0;

  @override
  String toString() {
    return 'RyzenInterfaceState.connected(field0: $field0)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RyzenInterfaceState_Connected &&
            (identical(other.field0, field0) || other.field0 == field0));
  }

  @override
  int get hashCode => Object.hash(runtimeType, field0);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$RyzenInterfaceState_ConnectedCopyWith<_$RyzenInterfaceState_Connected> get copyWith =>
      __$$RyzenInterfaceState_ConnectedCopyWithImpl<_$RyzenInterfaceState_Connected>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() disconnected,
    required TResult Function(RyzenCurrentValues field0) connected,
    required TResult Function(String field0) error,
  }) {
    return connected(field0);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? disconnected,
    TResult? Function(RyzenCurrentValues field0)? connected,
    TResult? Function(String field0)? error,
  }) {
    return connected?.call(field0);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? disconnected,
    TResult Function(RyzenCurrentValues field0)? connected,
    TResult Function(String field0)? error,
    required TResult orElse(),
  }) {
    if (connected != null) {
      return connected(field0);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RyzenInterfaceState_Disconnected value) disconnected,
    required TResult Function(RyzenInterfaceState_Connected value) connected,
    required TResult Function(RyzenInterfaceState_Error value) error,
  }) {
    return connected(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(RyzenInterfaceState_Disconnected value)? disconnected,
    TResult? Function(RyzenInterfaceState_Connected value)? connected,
    TResult? Function(RyzenInterfaceState_Error value)? error,
  }) {
    return connected?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RyzenInterfaceState_Disconnected value)? disconnected,
    TResult Function(RyzenInterfaceState_Connected value)? connected,
    TResult Function(RyzenInterfaceState_Error value)? error,
    required TResult orElse(),
  }) {
    if (connected != null) {
      return connected(this);
    }
    return orElse();
  }
}

abstract class RyzenInterfaceState_Connected implements RyzenInterfaceState {
  const factory RyzenInterfaceState_Connected(final RyzenCurrentValues field0) = _$RyzenInterfaceState_Connected;

  RyzenCurrentValues get field0;
  @JsonKey(ignore: true)
  _$$RyzenInterfaceState_ConnectedCopyWith<_$RyzenInterfaceState_Connected> get copyWith =>
      throw _privateConstructorUsedError;
}

/// @nodoc
abstract class _$$RyzenInterfaceState_ErrorCopyWith<$Res> {
  factory _$$RyzenInterfaceState_ErrorCopyWith(
          _$RyzenInterfaceState_Error value, $Res Function(_$RyzenInterfaceState_Error) then) =
      __$$RyzenInterfaceState_ErrorCopyWithImpl<$Res>;
  @useResult
  $Res call({String field0});
}

/// @nodoc
class __$$RyzenInterfaceState_ErrorCopyWithImpl<$Res>
    extends _$RyzenInterfaceStateCopyWithImpl<$Res, _$RyzenInterfaceState_Error>
    implements _$$RyzenInterfaceState_ErrorCopyWith<$Res> {
  __$$RyzenInterfaceState_ErrorCopyWithImpl(
      _$RyzenInterfaceState_Error _value, $Res Function(_$RyzenInterfaceState_Error) _then)
      : super(_value, _then);

  @pragma('vm:prefer-inline')
  @override
  $Res call({
    Object? field0 = null,
  }) {
    return _then(_$RyzenInterfaceState_Error(
      null == field0
          ? _value.field0
          : field0 // ignore: cast_nullable_to_non_nullable
              as String,
    ));
  }
}

/// @nodoc

class _$RyzenInterfaceState_Error implements RyzenInterfaceState_Error {
  const _$RyzenInterfaceState_Error(this.field0);

  @override
  final String field0;

  @override
  String toString() {
    return 'RyzenInterfaceState.error(field0: $field0)';
  }

  @override
  bool operator ==(dynamic other) {
    return identical(this, other) ||
        (other.runtimeType == runtimeType &&
            other is _$RyzenInterfaceState_Error &&
            (identical(other.field0, field0) || other.field0 == field0));
  }

  @override
  int get hashCode => Object.hash(runtimeType, field0);

  @JsonKey(ignore: true)
  @override
  @pragma('vm:prefer-inline')
  _$$RyzenInterfaceState_ErrorCopyWith<_$RyzenInterfaceState_Error> get copyWith =>
      __$$RyzenInterfaceState_ErrorCopyWithImpl<_$RyzenInterfaceState_Error>(this, _$identity);

  @override
  @optionalTypeArgs
  TResult when<TResult extends Object?>({
    required TResult Function() disconnected,
    required TResult Function(RyzenCurrentValues field0) connected,
    required TResult Function(String field0) error,
  }) {
    return error(field0);
  }

  @override
  @optionalTypeArgs
  TResult? whenOrNull<TResult extends Object?>({
    TResult? Function()? disconnected,
    TResult? Function(RyzenCurrentValues field0)? connected,
    TResult? Function(String field0)? error,
  }) {
    return error?.call(field0);
  }

  @override
  @optionalTypeArgs
  TResult maybeWhen<TResult extends Object?>({
    TResult Function()? disconnected,
    TResult Function(RyzenCurrentValues field0)? connected,
    TResult Function(String field0)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(field0);
    }
    return orElse();
  }

  @override
  @optionalTypeArgs
  TResult map<TResult extends Object?>({
    required TResult Function(RyzenInterfaceState_Disconnected value) disconnected,
    required TResult Function(RyzenInterfaceState_Connected value) connected,
    required TResult Function(RyzenInterfaceState_Error value) error,
  }) {
    return error(this);
  }

  @override
  @optionalTypeArgs
  TResult? mapOrNull<TResult extends Object?>({
    TResult? Function(RyzenInterfaceState_Disconnected value)? disconnected,
    TResult? Function(RyzenInterfaceState_Connected value)? connected,
    TResult? Function(RyzenInterfaceState_Error value)? error,
  }) {
    return error?.call(this);
  }

  @override
  @optionalTypeArgs
  TResult maybeMap<TResult extends Object?>({
    TResult Function(RyzenInterfaceState_Disconnected value)? disconnected,
    TResult Function(RyzenInterfaceState_Connected value)? connected,
    TResult Function(RyzenInterfaceState_Error value)? error,
    required TResult orElse(),
  }) {
    if (error != null) {
      return error(this);
    }
    return orElse();
  }
}

abstract class RyzenInterfaceState_Error implements RyzenInterfaceState {
  const factory RyzenInterfaceState_Error(final String field0) = _$RyzenInterfaceState_Error;

  String get field0;
  @JsonKey(ignore: true)
  _$$RyzenInterfaceState_ErrorCopyWith<_$RyzenInterfaceState_Error> get copyWith => throw _privateConstructorUsedError;
}
