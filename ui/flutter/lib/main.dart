import 'dart:async';
import 'dart:convert';
import 'dart:developer';
import 'dart:ffi';
import 'dart:typed_data';

import 'package:flutter/material.dart' hide Size;
import 'bridge_definitions.dart';

import 'ffi.io.dart';
export 'ffi.io.dart' show api;

void main() => runApp(const MyApp());

class MyApp extends StatefulWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  RyzenInterfaceState? state;

  @override
  void initState() {
    super.initState();
    api.init();
    api.createMpscStream().listen((newState) {
      setState(() {
        //print(json.encode(event));
        state = newState;

        /*final _value = switch (event) {
          RyzenInterfaceState_Disconnected() => print('disconnected'),
          RyzenInterfaceState_Connected(:final field0) => print(field0),
          RyzenInterfaceState_Error(:final field0) => print(field0),
        };*/
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return switch (state) {
      RyzenInterfaceState_Disconnected() => 
        const MaterialApp(
          home: Center(child: Text('disconnected'))
        ),
      RyzenInterfaceState_Connected(:final field0) => 
        MaterialApp(
          home: Center(child: Text(field0.toString())),
        ),
      RyzenInterfaceState_Error(:final field0) => MaterialApp(
        home: Center(child: Text(field0.toString())),
      ),
      null => const MaterialApp(
        home: Center(child: Text('null')),
      ),
    };
  }
}
