use std::time::Duration;

pub mod action;
pub mod state;

mod connected;

pub use action::*;
pub use state::*;

use crate::api::SINK;

#[tokio::main]
pub async fn init() {
    std::env::set_var("RUST_LOG_STYLE", "always");
    std::env::set_var("RUST_LOG", "debug");

    env_logger::init();

    let state_sink = loop {
        if let Some(s) = SINK.get() {
            break s.clone();
        }
        tokio::time::sleep(Duration::from_millis(10)).await;
    };
    info!("got state sink");
    state_sink.add(RyzenInterfaceState::Disconnected);

    loop {
        match ryzen_platform::dbus::connect().await {
            Ok(dbus_connection) => match dbus_connection.version().await {
                Ok(version) => {
                    info!("got version: {}", version);
                    connected::connected_loop(&dbus_connection, &state_sink).await;
                }
                Err(err) => {
                    error!("getting version failed: {}", err);
                    state_sink.add(RyzenInterfaceState::Disconnected);
                    tokio::time::sleep(Duration::from_millis(1000)).await;
                }
            },
            Err(err) => {
                error!("connecting failed: {}", err);
                state_sink.add(RyzenInterfaceState::Disconnected);
                tokio::time::sleep(Duration::from_millis(1000)).await;
            }
        }
    }
}
