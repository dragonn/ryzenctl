use std::time::Duration;

use flutter_rust_bridge::StreamSink;
use ryzen_platform::dbus::RyzenctlInterfaceProxy;

use super::RyzenInterfaceState;

pub async fn connected_loop(
    dbus_connection: &RyzenctlInterfaceProxy<'_>,
    state_sink: &StreamSink<RyzenInterfaceState>,
) -> anyhow::Result<()> {
    loop {
        state_sink.add(RyzenInterfaceState::Connected(dbus_connection.stats().await.unwrap()));
        tokio::time::sleep(Duration::from_secs(5)).await;
    }
}
