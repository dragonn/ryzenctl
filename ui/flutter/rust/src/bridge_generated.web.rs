use super::*;
// Section: wire functions

#[wasm_bindgen]
pub fn wire_init(port_: MessagePort) {
    wire_init_impl(port_)
}

#[wasm_bindgen]
pub fn wire_create_mpsc_stream(port_: MessagePort) {
    wire_create_mpsc_stream_impl(port_)
}

// Section: allocate functions

// Section: related functions

// Section: impl Wire2Api

// Section: impl Wire2Api for JsValue
