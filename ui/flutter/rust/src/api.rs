use std::sync::OnceLock;

use anyhow::{anyhow, Result};
use flutter_rust_bridge::{frb, StreamSink, ZeroCopyBuffer};

use crate::ryzenctl::RyzenInterfaceState;

pub fn init() {
    std::thread::spawn(|| {
        crate::ryzenctl::init();
    });
}

pub static SINK: OnceLock<StreamSink<RyzenInterfaceState>> = OnceLock::new();

pub fn create_mpsc_stream(s: StreamSink<RyzenInterfaceState>) {
    SINK.set(s).ok();
}
