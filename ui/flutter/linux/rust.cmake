# We include Corrosion inline here, but ideally in a project with
# many dependencies we would need to install Corrosion on the system.
# See instructions on https://github.com/AndrewGaspar/corrosion#cmake-install
# Once done, uncomment this line:
find_package(Corrosion REQUIRED)

corrosion_import_crate(MANIFEST_PATH ../rust/Cargo.toml CRATES ryzenui_flutter)

# Flutter-specific

set(CRATE_NAME "ryzenui_flutter")

target_link_libraries(${BINARY_NAME} PUBLIC ${CRATE_NAME})

list(APPEND PLUGIN_BUNDLED_LIBRARIES $<TARGET_FILE:${CRATE_NAME}-shared>)
